package sk.koncz.sally;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import sk.koncz.sally.datamodel.BinaryDocument;
import sk.koncz.sally.datamodel.Configuration;
import sk.koncz.sally.datamodel.Corpus;
import sk.koncz.sally.datamodel.Document;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Created by Peter.Koncz on 30. 7. 2016.
 */
public class SALLYFactory {
    Logger log = Logger.getLogger(SALLYFactory.class.getName());
    /**
     * Method to create Corpus
     * @return
     */
    public Corpus createCorpus(){
        return new Corpus();
    }

    /**
     * Method to create document
     * @param text
     * @return
     */
    public Document createDocument(String text){
        return new Document(text);
    }

    /**
     * Method to create classifier with default parameters
     * @return
     */
    public Classifier createClassifier() throws Exception {
        return new Classifier(new Configuration());
    }

    /**
     * Method to create classifier with own configuration
     * @param configuration the object with the configuration
     * @return
     */
    public Classifier createClassifier(Configuration configuration) throws Exception {
        return new Classifier(configuration);
    }

    /**
     * Method to get default configuration file
     * @return
     */
    public Configuration createConfiguration(){
        return new Configuration();
    }

    /**
     * Method to create configuration file based on the provided JSON configuration
     * @param path to the configuration file
     * @return
     */
    public Configuration createConfiguration(Path path) throws Exception {
        Gson gson = new Gson();
        Configuration configuration = new Configuration();
        BufferedReader br = null;
        br = new BufferedReader(new FileReader(path.toFile()));
        StringBuilder sb = new StringBuilder();
        String line = br.readLine();
        while (line != null) {
            sb.append(line);
            sb.append(System.lineSeparator());
            line = br.readLine();
        }
        br.close();
        String everything = sb.toString();
        configuration = gson.fromJson(everything, Configuration.class);
        //check the correctness of the config settings
        checkConfig(configuration);
        return configuration;
    }
    //check configuration
    private void checkConfig(Configuration configuration) throws Exception {
        if (!(configuration.getWeightingMethod().equals("OWN")||configuration.getWeightingMethod().equals("IG"))) {
            throw new Exception("The method must be OWN or IG!");
        }
        if (!(configuration.getMaxNGramSize()>0&&configuration.getMaxNGramSize()<6)) {
            throw new ArithmeticException("The maxNGramSize should be from interval <1;5>!");
        }
        if ((configuration.getMaxWeightThreshold()<0||configuration.getMinWeightThreshold()>0)) {
            throw new ArithmeticException("The maxWeightThreshold should be from interval <0;1> and the minWeightThreshold from interval <0;-1>!");
        }
        if (!(configuration.getSolver().equals("L1R_LR")||configuration.getSolver().equals("L2R_LR"))) {
            throw new Exception("The solver must be L1R_LR or L2R_LR!");
        }
        if (configuration.getHighlightText()&&!configuration.getFilterOverliedNGrams()) {
            throw new Exception("The text highlighting works only with enabled filterring of overlalied NGrams!");
        }
    }
}
