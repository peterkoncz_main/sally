package sk.koncz.sally;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import sk.koncz.sally.datamodel.*;
import sk.koncz.sally.datamodel.BinaryDocument;
import de.bwaldvogel.liblinear.FeatureNode;
import de.bwaldvogel.liblinear.Linear;
import de.bwaldvogel.liblinear.Parameter;
import de.bwaldvogel.liblinear.Problem;
import de.bwaldvogel.liblinear.SolverType;

public class BinaryClassifier{
	private BinaryModel model;
	private BinaryCorpus corpus;
	private Double weightHighlightThreshold = 0.0;
	private final String space = "_specialSpaceString_";
	private Configuration configuration;
	/**
	 * Constructor of the classifier
	 * @param configuration
	 * @throws Exception
	 */
	BinaryClassifier(Configuration configuration) throws Exception {
		this.configuration=configuration;
	}
	/**
	 * Constructor of the classifier with default configuration
	 * @throws Exception
	 */
	BinaryClassifier() throws Exception {
		this.configuration=new Configuration();
	}
	/**
	 * The method to build the model
	 * @param corpus
	 * @return 
	 * @throws Exception
	 */
	BinaryModel buildModel(BinaryCorpus corpus) throws Exception {
		this.corpus = corpus;
		this.model = new BinaryModel();
		corpusStatistics();
		segmentation();
		nGramGeneration();
		getFeatures();
		getTresholds();
		filterOutFeatures();
		this.model.setConfiguration(configuration);
		this.corpus = null;
		return this.model;
	}
	/**
	 * Method to apply the model 
	 * @param corpus
	 * @param model
	 * @return
	 */
	BinaryCorpus applyModel(BinaryCorpus corpus, BinaryModel model) {
		this.corpus = corpus;
		this.model = model;
		this.configuration = model.getConfiguration();
		applySegmentation();
		applyNGramGeneration();
		classifyDocuments();
		highlightText();
		return this.corpus;
	}
	/**
	 * This method is used to evaluate the corpus
	 * @param corpus
	 */
	void evaluateCorpus (BinaryCorpus corpus) {
		this.corpus = corpus;
		Double truePositive = 0.0;
		Double trueNegative = 0.0;
		Double falseNegative = 0.0;
		Double falsePositive = 0.0;
		for (BinaryDocument document : corpus.getDocuments()) {
			if (document.getDocClass()==1&&document.getModelBasedClass()==1) {
				truePositive++;
			} else if(document.getDocClass()==0&&document.getModelBasedClass()==1){
				falsePositive++;
			} else if(document.getDocClass()==1&&document.getModelBasedClass()==0) {
				falseNegative++;
			} else if(document.getDocClass()==0&&document.getModelBasedClass()==0){
				trueNegative++;
			}
		}
		Double recallPositive = truePositive/(truePositive+falseNegative);
		Double precisionPositive = 0.0;
		if (truePositive+falsePositive!=0) {
			precisionPositive = truePositive/(truePositive+falsePositive);
		}
		Double recallNegative = trueNegative/(trueNegative+falsePositive);
		Double precisionNegative = 0.0;
		if (trueNegative+falseNegative!=0) {
			precisionNegative = trueNegative/(trueNegative+falseNegative);
		}
		Double accuracy = (truePositive+trueNegative)/(truePositive+trueNegative+falsePositive+falseNegative);
		corpus.setRecallPositive(recallPositive);
		corpus.setRecallNegative(recallNegative);
		corpus.setPrecisionPositive(precisionPositive);
		corpus.setPrecisionNegative(precisionNegative);
		corpus.setAccuracy(accuracy);
	}
	private void highlightText() {
		if (configuration.getHighlightText()) {
			return;
		}
		for (BinaryDocument doc : corpus.getDocuments()) {
			HashMap<Integer, Double> wordWeights = doc.getWordWeights();
			String[] tokens = doc.getPreprocessedText().split(space);
			String text = "";
			int stringNumber = 0;
			for (String token : tokens) {
				stringNumber++;
				if (wordWeights.containsKey(stringNumber)) {
					Double weight = wordWeights.get(stringNumber);
					String color = (weight>0) ? "0,255,0" : "255,0,0"; 
					if (Math.abs(weight)>weightHighlightThreshold) {
						text = text+"<span style=\"background-color:rgba("+color+","+Math.abs(weight)+")\"> "+token+"</span>";
					} else {
						text = text+" "+token;
					}
				} else {
					text = text+" "+token;
				}
			}
			doc.setHighlightedText(text);
		}
	}
	private Double getFeaturesSum(BinaryDocument doc, List<FeatureOccurrence> listOfIdentifiedFeatures) {
		Double featurestSum = 0.0;
		Double positiveFeaturesSum = 0.0;
		Double negativeFeaturesSum = 0.0;
		int usedFeatures = 0;
		int usedPositiveFeatures = 0;
		int usedNegativeFeatures = 0;
		HashMap<Integer, Double> wordWeights = new HashMap<Integer, Double>();
		if (configuration.getFilterOverliedNGrams()) {
			Collections.sort(listOfIdentifiedFeatures, new Comparator<FeatureOccurrence>() {
				@Override
				public int compare(FeatureOccurrence p1, FeatureOccurrence p2) {
					double delta= Math.abs(p2.getWeight()) - Math.abs(p1.getWeight());
				    if(delta > 0) return 1;
				    if(delta < 0) return -1;
				    return 0; // Ascending
				}
			});
			Set<Integer> coveredWords = new HashSet<>();
			for (FeatureOccurrence featureOccurrence : listOfIdentifiedFeatures) {
				int sizeOfCoveredWordsBeforeAdding = coveredWords.size();
				Set<Integer>oldCoveredWords = coveredWords;
				coveredWords.addAll(featureOccurrence.getWords());
				int sizeOfCoveredWordsAfterAdding = coveredWords.size();
				//all the words should be new
				if (featureOccurrence.getWords().size()==sizeOfCoveredWordsAfterAdding-sizeOfCoveredWordsBeforeAdding) {
					featurestSum = featurestSum + featureOccurrence.getWeight();
					if (featureOccurrence.getWeight()>0) {
						positiveFeaturesSum = positiveFeaturesSum + featureOccurrence.getWeight();
						usedPositiveFeatures++;
					} else if(featureOccurrence.getWeight()<0) {
						negativeFeaturesSum = negativeFeaturesSum + featureOccurrence.getWeight();
						usedNegativeFeatures++;
					}
					List<Integer> words = featureOccurrence.getWords();
					if (configuration.getHighlightText()) {
						for (Integer word : words) {
							wordWeights.put(word, featureOccurrence.getWeight());
						}
					}
				} else {
					coveredWords = oldCoveredWords;
				}
			}
			usedFeatures = coveredWords.size();
		} else {
			for (FeatureOccurrence featureOccurrence : listOfIdentifiedFeatures) {
				featurestSum = featurestSum + featureOccurrence.getWeight();
				if (featureOccurrence.getWeight()>0) {
					positiveFeaturesSum = positiveFeaturesSum + featureOccurrence.getWeight();
					usedPositiveFeatures++;
				} else if(featureOccurrence.getWeight()<0) {
					negativeFeaturesSum = negativeFeaturesSum + featureOccurrence.getWeight();
					usedNegativeFeatures++;
				}
			}
			usedFeatures = listOfIdentifiedFeatures.size();
		}
		doc.setFeaturesSum(featurestSum);
		doc.setPositiveFeaturesSum(positiveFeaturesSum);
		doc.setNegativeFeaturesSum(negativeFeaturesSum);
		doc.setWordWeights(wordWeights);
		doc.setUsedFeatures(usedFeatures);
		doc.setUsedPositiveFeatures(usedPositiveFeatures);
		doc.setUsedNegativeFeatures(usedNegativeFeatures);
		return featurestSum;
	}
	//get the corpus statistics of classes
	private void corpusStatistics() throws Exception {
		int nPositive = 0; //the number of positive documents
		int nNegative = 0; //the number of negative documents
		for (BinaryDocument document : corpus.getDocuments()) {
			if (document.getDocClass()==1) {
				nPositive+=1;
			} else if(document.getDocClass()==0) {
				nNegative+=1;
			} else {
			 	throw new Exception("All the documents from the traning corpus should have class 1 or 0!"); 
			}
		}
		if (nPositive<10||nNegative<10) {
			throw new Exception("Each class should contain at least 10 examples!"); 
		}
		corpus.setNumPositiveDocuments(nPositive);
		corpus.setNumNegativeDocuments(nNegative);
		corpus.setTruePositiveWeight(((double)nNegative/(double)(nPositive+nNegative)));
		corpus.setTrueNegativeWeight(((double)nPositive/(double)(nPositive+nNegative)));
		corpus.setFalseNegativeWeight(((double)nNegative/(double)(nPositive+nNegative)));
		corpus.setFalsePositiveWeight(((double)nPositive/(double)(nPositive+nNegative)));
		//here we need to implement control of corpus parameters
	}
	//filter out features based on tresholds
	private void filterOutFeatures() {
		for(Iterator<Map.Entry<String, Feature>> it = model.getFeatures().entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, Feature> entry = it.next();
			if(entry.getValue().getWeight()>0) {
				if (entry.getValue().getWeight()<=model.getThresholdWeightsPositive()) {
					it.remove();
				}
			} else if (entry.getValue().getWeight()<0) {
				if (entry.getValue().getWeight()>=model.getThresholdWeightsNegative()) {
					it.remove();
				}
			}
		}
	}
	private void getTopFeatures(HashMap<String, Feature> features, int n) {
		// Convert Map to List
		List<Map.Entry<String, Feature>> list = new LinkedList<Map.Entry<String, Feature>>(features.entrySet());
		// Sort list with comparator, to compare the Map values
		Collections.sort(list, new Comparator<Map.Entry<String, Feature>>() {
			public int compare(Map.Entry<String, Feature> o1, Map.Entry<String, Feature> o2) {
				return Double.compare((Math.abs(o2.getValue().getWeight())),(Math.abs(o1.getValue().getWeight())));
			}
		});
		// Convert sorted map back to a Map
		Map<String, Feature> sortedMap = new LinkedHashMap<String, Feature>();
		for (Iterator<Map.Entry<String, Feature>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Feature> entry = it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		//get the top n features
		int i = 0;
		for(Iterator<Map.Entry<String, Feature>> it = features.entrySet().iterator(); it.hasNext(); ) {
			it.next();
			i++;
			if (i>n) {
				it.remove();
			}
		}
	}
	//the probability of positive example based on logistic regression
	private Double getProbability(Double beta0, Double beta1, Double beta2, BinaryDocument doc) {
		return 1/(1+Math.exp(-(beta0+beta1*doc.getFeaturesSum()+beta2*(doc.getPositiveFeaturesSum()-doc.getNegativeFeaturesSum()))));
	}
	private Double[] getLogisticRegression() {
		Problem problem = new Problem();
		problem.l = corpus.getDocuments().size(); // number of training examples
		problem.n = 3; // number of features
		FeatureNode[][] trainingSet = new FeatureNode[corpus.getDocuments().size()][];
		double[] classes = new double[corpus.getDocuments().size()];
		int i = 0;
		for (BinaryDocument document : corpus.getDocuments()) {
			FeatureNode[] fn = {new FeatureNode(1, 1), new FeatureNode(2,  document.getFeaturesSum()), new FeatureNode(3, (document.getPositiveFeaturesSum()-document.getNegativeFeaturesSum()))};
			trainingSet[i] = fn;
			classes[i] = document.getDocClass();
			i++;		
		}
		problem.y = classes;
		problem.x = trainingSet;
		Linear.resetRandom();
		Linear.disableDebugOutput();
		SolverType solver = null;
		if (configuration.getSolver().equals("L1R_LR")) {
			solver = SolverType.L1R_LR;
		} else if (configuration.getSolver().equals("L2R_LR")) {
			solver = SolverType.L2R_LR;
		}
		Parameter parameter = new Parameter(solver, configuration.getC(), configuration.getEps());
		de.bwaldvogel.liblinear.Model lrModel = Linear.train(problem, parameter);
		double[] weights = lrModel.getFeatureWeights();
		return new Double[]{weights[0], weights[1], weights[2]};
	}
	private void nGramGeneration() {
		Set<String> relevantTokens = filterTokens();
		getRelevantNGrams(relevantTokens);
		for (BinaryDocument document : corpus.getDocuments()) {
			NGramGenerator nGramGenerator = new NGramGenerator(space, model.getCharacterNGrams(), configuration.getMaxNGramSize(), configuration.getNGramPatterns());
			document.setStringNGrams(nGramGenerator.getStringNGrams(document.getPreprocessedText()));
		}
	}
	//this method is supposed to remove irrelevant tokens before generation of string n-gram
	private Set<String> filterTokens() {
		HashMap<String, Integer> tokensCountPositive = new HashMap<>();
		HashMap<String, Integer> tokensCountNegative = new HashMap<>();
		Set<String> relevantTokens = new HashSet<>(); 
		for (BinaryDocument document : corpus.getDocuments()) {
			//the occurrence is considered only once per document
			Set<String> documentTokens = new HashSet<>();
			String[] tokens = document.getPreprocessedText().split(space);
			for (String token : tokens) {
				if (!documentTokens.contains(token)) {
					documentTokens.add(token);
					if (document.getDocClass()==1) {
						if (tokensCountPositive.containsKey(token)) {
							tokensCountPositive.put(token, tokensCountPositive.get(token)+1); 
						} else {
							tokensCountPositive.put(token, 1);
						}
					} else {
						if (tokensCountNegative.containsKey(token)) {
							tokensCountNegative.put(token, tokensCountNegative.get(token)+1); 
						} else {
							tokensCountNegative.put(token, 1);
						}
					} 
				}
			}
		}
		Set<String> keys = tokensCountPositive.keySet();
		for (String key : keys) {
			if (tokensCountPositive.get(key)>1) {
				relevantTokens.add(key);
			}
		}
		keys = tokensCountNegative.keySet();
		for (String key : keys) {
			if (tokensCountNegative.get(key)>1) {
				relevantTokens.add(key);
			}
		}
		return relevantTokens;
	}
	//get the combinations of thresholds for positive and negative weights
	private List<TresholdsCombination> getWeightsTresholds(HashMap<String, Feature> features) {
		List<TresholdsCombination> tresholdsCombinations = new ArrayList<TresholdsCombination>();
		if (configuration.getWeightsTresholdOptimization()) {
			Set<Integer> weightsTresholdsListPostive = new HashSet<Integer>();
			Set<Integer> weightsTresholdsListNegative = new HashSet<Integer>();
			Set<String> keys = features.keySet();
			for (String key : keys) {
				Integer weight = (int) (features.get(key).getWeight()*100);
				if (weight>0) {
					weightsTresholdsListPostive.add(weight);
				} else if(weight<0) {
					weightsTresholdsListNegative.add(weight);
				}
			}
			for (Integer positive : weightsTresholdsListPostive) {
				Double maxWeightThreshold = configuration.getMaxWeightThreshold()*100;
				if (positive>maxWeightThreshold.intValue()) {
					continue;
				}
				for (Integer negative : weightsTresholdsListNegative) {
					Double minWeightThreshold = configuration.getMinWeightThreshold()*100;
					if (negative<minWeightThreshold.intValue()) {
						continue;
					}
					TresholdsCombination tresholdCombination = new TresholdsCombination();
					tresholdCombination.setPositiveTreshold((positive.doubleValue()/100));
					tresholdCombination.setNegativeTreshold((negative.doubleValue()/100));
					tresholdCombination.setFeaturesNum(getFeaturesNumber(tresholdCombination.getPositiveTreshold(), tresholdCombination.getNegativeTreshold()));
					tresholdsCombinations.add(tresholdCombination);
				}
			}
		}
		TresholdsCombination tresholdCombination = new TresholdsCombination();
		tresholdCombination.setPositiveTreshold(0.0);
		tresholdCombination.setNegativeTreshold(0.0);
		tresholdCombination.setFeaturesNum(model.getFeatures().entrySet().size());
		tresholdsCombinations.add(tresholdCombination);
		Collections.sort(tresholdsCombinations, new Comparator<TresholdsCombination>() {
			@Override 
			public int compare(TresholdsCombination p1, TresholdsCombination p2) {
				int delta= p2.getFeaturesNum() - p1.getFeaturesNum();
			    if(delta > 0) return 1;
			    if(delta < 0) return -1;
			    return 0; // Ascending
			}
		});
		return tresholdsCombinations;
	}
	//tokenization THIS HAVE TO BE OPTIMIZED!
	private String[] tokenize (String text) {
		return text.replace(".", " .").replace(",", " ,").replace("?", " ?").replace("!"," !").toLowerCase().split("[\\s]");
	}
	//get the weight of string n-grams
	private void getFeatures() {
		for (BinaryDocument document : corpus.getDocuments()) {
			//we need to count the features only once for a document
			Set<String> nGrams = new HashSet<>();
			for (StringNGram stringNGram : document.getStringNGrams()) {
				if (!nGrams.contains(stringNGram.getnGram())) {
					nGrams.add(stringNGram.getnGram());
					if (model.getFeatures().containsKey(stringNGram.getnGram())) {
						Feature feature = model.getFeatures().get(stringNGram.getnGram());
						if (document.getDocClass()==1) {
							feature.setOccurenceClassA(feature.getOccurenceClassA()+1);
						} else {
							feature.setOccurenceClassB(feature.getOccurenceClassB()+1);
						}
					} else {
						Feature feature = new Feature();
						if (document.getDocClass()==1) {
							feature.setOccurenceClassA(1);
						} else {
							feature.setOccurenceClassB(1);
						}
						model.getFeatures().put(stringNGram.getnGram(), feature);
					}
				}
			}
		}
		this.getWeights(model.getFeatures(), corpus.getNumPositiveDocuments(), corpus.getNumNegativeDocuments());
	}
	private void getTresholds() {
		//Compute the weights for -1 positive and -1 negative class, to have the weights without considering the actual document which could be positive or negative
		HashMap<String, Feature> minusOnePositiveFeatures = new HashMap<String, Feature>();
		HashMap<String, Feature> minusOneNegativeFeatures = new HashMap<String, Feature>();
		Set<String> keys = model.getFeatures().keySet();
		for (String key : keys) {
			Feature feature = model.getFeatures().get(key);
			Feature minusOnePositiveFeature = new Feature();
			if (feature.getOccurenceClassA()!=0) {
				minusOnePositiveFeature.setOccurenceClassA(feature.getOccurenceClassA()-1);
			}
			minusOnePositiveFeature.setOccurenceClassB(feature.getOccurenceClassB());
			minusOnePositiveFeatures.put(key, minusOnePositiveFeature);
			Feature minusOneNegativeFeature = new Feature();
			minusOneNegativeFeature.setOccurenceClassA(feature.getOccurenceClassA());
			if (feature.getOccurenceClassB()!=0) {
				minusOneNegativeFeature.setOccurenceClassB(feature.getOccurenceClassB()-1);
			}
			minusOneNegativeFeatures.put(key, minusOneNegativeFeature);
		}
		getWeights(minusOnePositiveFeatures, corpus.getNumPositiveDocuments()-1, corpus.getNumNegativeDocuments());
		getWeights(minusOneNegativeFeatures,corpus.getNumPositiveDocuments(), corpus.getNumNegativeDocuments()-1);
		List<TresholdsCombination> weightsTresholds = new ArrayList<TresholdsCombination>();
		weightsTresholds = getWeightsTresholds(model.getFeatures());
		Double bestFit = 0.0;
		int downgradeCount = 0;
		for (TresholdsCombination tresholdsCombination : weightsTresholds) {
			if (downgradeCount>configuration.getMaxDowngradeIterations()) {
				break;
			}
			//Compute the features sum for each document
			for (BinaryDocument doc : corpus.getDocuments()) {
				List<FeatureOccurrence> listOfIdentifiedFeatures = new ArrayList<>();
				for (StringNGram stringNGram : doc.getStringNGrams()) {
					if (doc.getDocClass()==1) {
						//first we need to check whether the feature stringNGram is from positive features
						if (minusOnePositiveFeatures.containsKey(stringNGram.getnGram())) {
							//then we need to check whether the weight is bigger than the treshold
							if (minusOnePositiveFeatures.get(stringNGram.getnGram()).getWeight()>tresholdsCombination.getPositiveTreshold()||minusOnePositiveFeatures.get(stringNGram.getnGram()).getWeight()<tresholdsCombination.getNegativeTreshold()) {
								FeatureOccurrence featureOccurence = new FeatureOccurrence();
								featureOccurence.setWeight(minusOnePositiveFeatures.get(stringNGram.getnGram()).getWeight());
								featureOccurence.setWords(stringNGram.getWords());
								listOfIdentifiedFeatures.add(featureOccurence);
							} 
						}
					} else {
						//first we need to check whether the feature stringNGram is from negative features
						if (minusOneNegativeFeatures.containsKey(stringNGram.getnGram())) {
							//then we need to check whether the weight is bigger than the treshold
							if (minusOneNegativeFeatures.get(stringNGram.getnGram()).getWeight()>tresholdsCombination.getPositiveTreshold()||minusOneNegativeFeatures.get(stringNGram.getnGram()).getWeight()<tresholdsCombination.getNegativeTreshold()) {
								FeatureOccurrence featureOccurence = new FeatureOccurrence();
								featureOccurence.setWeight(minusOneNegativeFeatures.get(stringNGram.getnGram()).getWeight());
								featureOccurence.setWords(stringNGram.getWords());
								listOfIdentifiedFeatures.add(featureOccurence);
							}
						}
					}
				}
				doc.setFeaturesSum(getFeaturesSum(doc, listOfIdentifiedFeatures));
			}
			//get the logistic regression
			Double[] lr = getLogisticRegression();
			Double fit = evaluateTrainSet(lr[0], lr[1], lr[2]);
			if (fit>=bestFit-configuration.getAccuracyTollerance()) {
				if (fit>=bestFit) {
					bestFit=fit;
				}
				model.setThresholdWeightsPositive(tresholdsCombination.getPositiveTreshold());
				model.setThresholdWeightsNegative(tresholdsCombination.getNegativeTreshold());
				//model.setThreshold(lr[0]);
				model.setBeta0(lr[0]);
				model.setBeta1(lr[1]);
				model.setBeta2(lr[2]);
				model.setEstimatedAccuracy(fit);
				downgradeCount=0;
			} else {
				downgradeCount++;
			}
		}
	}
	private int getFeaturesNumber(Double positiveTreshold, Double negativeTreshold) {
		int featuresNum = 0;
		for(Iterator<Map.Entry<String, Feature>> it = model.getFeatures().entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, Feature> entry = it.next();
			if(entry.getValue().getWeight()>0) {
				if (entry.getValue().getWeight()>positiveTreshold) {
					featuresNum++;
				}
			} else if (entry.getValue().getWeight()<0) {
				if (entry.getValue().getWeight()<negativeTreshold) {
					featuresNum++;
				}
			}
		}
		return featuresNum;
	}
	//this method evaluates fit(accuracy) for the given treshold  
	private Double evaluateTrainSet(Double beta0, Double beta1, Double beta2) {
		Double truePositive = 0.0;
		Double trueNegative = 0.0;
		Double falseNegative = 0.0;
		Double falsePositive = 0.0;
		for (BinaryDocument doc : corpus.getDocuments()) {
			Double probability = getProbability(beta0, beta1, beta2, doc);
			if (doc.getDocClass()==1) {
				if (probability>0.5) {
					truePositive++;
				} else {
					falseNegative++;
				}
			}else {
				if (probability>0.5) {
					falsePositive++;
				} else {
					trueNegative++;
				}
			}
		}
		Double accuracy = (truePositive*corpus.getTruePositiveWeight()+trueNegative*corpus.getTrueNegativeWeight())/(truePositive*corpus.getTruePositiveWeight()+trueNegative*corpus.getTrueNegativeWeight()+falsePositive*corpus.getFalsePositiveWeight()+falseNegative*corpus.getFalseNegativeWeight());
		return accuracy;
	}
	//method to get relevant n-grams for features
	private void getRelevantNGrams(Set<String> relevantTokens) {
		HashMap<String, Feature> nGramOccurences = new HashMap<>();
		for (BinaryDocument document : corpus.getDocuments()) {
			String[] tokens = document.getPreprocessedText().split(space);
			List<String> filteredTokens = new ArrayList<>();
			for (String token : tokens) {
				if (relevantTokens==null) {
					filteredTokens.add(token);
				} else if(relevantTokens.contains(token)) {
						filteredTokens.add(token);
				}
			}
			NGramGenerator nGramGenerator = new NGramGenerator(space, model.getCharacterNGrams(), configuration.getMaxNGramSize(), configuration.getNGramPatterns());
			Set<String> composedWords = nGramGenerator.getWords(filteredTokens);
			for (String composedWord : composedWords) {
				if (nGramOccurences.containsKey(composedWord)) {
					Feature nGram = nGramOccurences.get(composedWord);
					if (document.getDocClass()==1) {
						nGram.setOccurenceClassA(nGram.getOccurenceClassA()+1);
					}
					if (document.getDocClass()==0) {
						nGram.setOccurenceClassB(nGram.getOccurenceClassB()+1);
					}
					nGramOccurences.put(composedWord, nGram);
				} else {
					Feature nGram = new Feature();
					if (document.getDocClass()==1) {
						nGram.setOccurenceClassA(1);
					}
					if (document.getDocClass()==0) {
						nGram.setOccurenceClassB(1);
					}
					nGramOccurences.put(composedWord, nGram);
				}
			}
		}
		for(Iterator<Map.Entry<String, Feature>> it = nGramOccurences.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, Feature> entry = it.next();
			if((entry.getValue().getOccurenceClassA()+entry.getValue().getOccurenceClassB())==1||entry.getValue().getOccurenceClassA()==entry.getValue().getOccurenceClassB()) {
				it.remove();
			}
		}
		this.model.setCharacterNGrams(nGramOccurences);
	}
	//method which computes the weights of the words in HashMap based on the formula suggested in Koncz and Paralic [http://dx.doi.org/10.1109/INES.2011.5954773]
	private void getWeights(HashMap<String, Feature> hashmap, int nPositive, int nNegative) {
		Set<String> keys = hashmap.keySet();
		for (String key : keys) {
			Feature word = hashmap.get(key);
			//Double weight = Math.abs(Math.log(word.getOccurenceClassA()+1)-Math.log(word.getOccurenceClassB()+1))/Math.log(nPositive+nNegative/2+1);
			Double weight = 0.0;
			if ((word.getOccurenceClassA()+word.getOccurenceClassB())!=1&&word.getOccurenceClassA()!=word.getOccurenceClassB()) {
				if (configuration.getWeightingMethod().equals("OWN")) {
					weight = Math.abs((Math.log((double)(word.getOccurenceClassA()+1)/(nPositive+1))/Math.log(nPositive+1))-(Math.log((double)(word.getOccurenceClassB()+1)/(nNegative+1))/Math.log(nNegative+1)));
				} else if(configuration.getWeightingMethod().equals("IG")) {
					Double s = (double) corpus.getNumPositiveDocuments()+corpus.getNumNegativeDocuments();//all documents in the sample
					Double entropy = -((corpus.getNumPositiveDocuments()/s)*(Math.log(corpus.getNumPositiveDocuments()/s))/Math.log(2)+(corpus.getNumNegativeDocuments()/s)*(Math.log(corpus.getNumNegativeDocuments()/s))/Math.log(2));
					Double s1 = (double) (word.getOccurenceClassA()+word.getOccurenceClassB());//all documents with the occurence of particular feature
					Double s0 = (double) s-s1;//all documents without the occurence of particular feature
					Double s11 = (double) word.getOccurenceClassA();//documents with feature from positive class
					Double s10 = (double) word.getOccurenceClassB();//document with feature from negative class
					Double s01 = (double) corpus.getNumPositiveDocuments()-word.getOccurenceClassA();////document without feature from positive class
					Double s00 = (double) corpus.getNumNegativeDocuments()-word.getOccurenceClassB();//document without feature from negative class
					Double s000 = ((Double.isNaN(s00/s0)||s00/s0==0) ? 0 : (s00/s0)*Math.log(s00/s0)/Math.log(2));
					Double s010 = ((Double.isNaN(s01/s0)||s01/s0==0) ? 0 : (s01/s0)*Math.log(s01/s0)/Math.log(2));
					Double s101 = ((Double.isNaN(s10/s1)||s10/s1==0) ? 0 : (s10/s1)*Math.log(s10/s1)/Math.log(2));
					Double s111 = ((Double.isNaN(s11/s1)||s11/s1==0) ? 0 : (s11/s1)*Math.log(s11/s1)/Math.log(2));
					Double first = (s0/s)*(-s000-s010);
					Double second = (s1/s)*(-s101-s111);
					weight = entropy-first-second;
					if (weight<0) {
						weight=0.0;
					}
				}
			}
			//System.out.println("A: "+word.getOccurenceClassA()+"B: "+word.getOccurenceClassB()+" "+weight);
			if (word.getOccurenceClassA()<word.getOccurenceClassB()) {
				weight=weight*-1;
			}
			word.setWeight(weight);
		}
	}
	private void segmentation(){
		//if the segmentation is disabled then do just tokenization
		if (!configuration.getSegmentation()) {
			for (BinaryDocument document : corpus.getDocuments()) {
				document.setTokenizedText(tokenize(document.getText()));
				String preprocessedText = "";
				for (String word : document.getTokenizedText()) {
					preprocessedText = preprocessedText+word+space;
				}
				document.setPreprocessedText(preprocessedText);
			}
			return;
		}
		//Populate the list of prefixes, suffixes and original whole tokens
		HashMap<String, Feature> prefixes = new HashMap<>();
		HashMap<String, Feature> suffixes = new HashMap<>();
		HashMap<String, Feature> wholeTokens = new HashMap<>();
		for (BinaryDocument doc : corpus.getDocuments()) {
			//Each prefix, suffix and whole word is counted only once per document
			Set<String> presentPrefixes = new HashSet<String>();
			Set<String> presentSuffixes = new HashSet<String>();
			Set<String> presentWholeTokens = new HashSet<String>();
			doc.setTokenizedText(tokenize(doc.getText()));
			for (String token : doc.getTokenizedText()) {
				if (!presentWholeTokens.contains(token)) {
					presentWholeTokens.add(token);	
					if (doc.getDocClass()==1) {
						if (wholeTokens.containsKey(token)) {
							Feature feature = wholeTokens.get(token);
							feature.setOccurenceClassA(feature.getOccurenceClassA()+1);
							wholeTokens.put(token, feature);
						}
						else {
							Feature feature = new Feature();
							feature.setOccurenceClassA(1);
							wholeTokens.put(token, feature);
						}
					} else if(doc.getDocClass()==0) {
						if (wholeTokens.containsKey(token)) {
							Feature feature = wholeTokens.get(token);
							feature.setOccurenceClassB(feature.getOccurenceClassB()+1);
							wholeTokens.put(token, feature);
						}
						else {
							Feature feature = new Feature();
							feature.setOccurenceClassB(1);
							wholeTokens.put(token, feature);
						}
					}
					//Generate the prefixes and suffixes
					for (int i = 1; i <= 4; i++) {
						if (token.length()>i) {
							String prefix = token.substring(0, i);
							String suffix = token.substring(token.length()-i);
							if (doc.getDocClass()==1) {
								if (!presentPrefixes.contains(prefix)) {
									presentPrefixes.add(prefix);
									if (prefixes.containsKey(prefix)) {
										Feature feature = prefixes.get(prefix);
										feature.setOccurenceClassA(feature.getOccurenceClassA()+1);
										prefixes.put(prefix, feature);
									}
									else {
										Feature feature = new Feature();
										feature.setOccurenceClassA(1);
										prefixes.put(prefix, feature);
									}
								}
								if (!presentSuffixes.contains(suffix)) {
									presentSuffixes.add(suffix);
									if (suffixes.containsKey(suffix)) {
										Feature feature = suffixes.get(suffix);
										feature.setOccurenceClassA(feature.getOccurenceClassA()+1);
										suffixes.put(suffix, feature);
									}
									else {
										Feature feature = new Feature();
										feature.setOccurenceClassA(1);
										suffixes.put(suffix, feature);
									}
								}
							} else if(doc.getDocClass()==0) {
								if (!presentPrefixes.contains(prefix)) {
									presentPrefixes.add(prefix);
									if (prefixes.containsKey(prefix)) {
										Feature feature = prefixes.get(prefix);
										feature.setOccurenceClassB(feature.getOccurenceClassB()+1);
										prefixes.put(prefix, feature);
									}
									else {
										Feature feature = new Feature();
										feature.setOccurenceClassB(1);
										prefixes.put(prefix, feature);
									}
								}
								if (!presentSuffixes.contains(suffix)) {
									presentSuffixes.add(suffix);
									if (suffixes.containsKey(suffix)) {
										Feature feature = suffixes.get(suffix);
										feature.setOccurenceClassB(feature.getOccurenceClassB()+1);
										suffixes.put(suffix, feature);
									}
									else {
										Feature feature = new Feature();
										feature.setOccurenceClassB(1);
										suffixes.put(suffix, feature);
									}
								}
							}
						}
					}
				}
			}
		}
		//Compute the weights of the prefixes, suffixes and whole tokens
		getWeights(prefixes, corpus.getNumPositiveDocuments(), corpus.getNumNegativeDocuments());
		getTopFeatures(prefixes, configuration.getNumTopPrefixes());
		getWeights(suffixes, corpus.getNumPositiveDocuments(), corpus.getNumNegativeDocuments());
		getTopFeatures(suffixes, configuration.getNumTopSuffixes());
		getWeights(wholeTokens, corpus.getNumPositiveDocuments(), corpus.getNumNegativeDocuments());
		//Based on the weights we will do the segmentation
		for (BinaryDocument doc : corpus.getDocuments()) {
			String preprocessedText = "";
			for (String token : doc.getTokenizedText()) {
				Double biggestWeight = 0.0;
				String segmentedToken = token;
				if (wholeTokens.containsKey(token)) {
					biggestWeight=Math.abs(wholeTokens.get(token).getWeight());
					segmentedToken = token;
				}
				for (int i = 1; i <= 4; i++) {
					if (token.length()>i) {
						String prefix = token.substring(0, i);
						if (prefixes.containsKey(prefix)) {
							if (Math.abs(prefixes.get(prefix).getWeight())>biggestWeight) {
								biggestWeight = prefixes.get(prefix).getWeight();
								segmentedToken = prefix+space+token.substring(i);
							}
						}
						String suffix = token.substring(token.length()-i);
						if (suffixes.containsKey(suffix)) {
							if (Math.abs(suffixes.get(suffix).getWeight())>biggestWeight) {
								biggestWeight = suffixes.get(suffix).getWeight();
								segmentedToken = token.substring(0,token.length()-i)+space+suffix;
							}
						}
					}
				}
				preprocessedText = preprocessedText+segmentedToken+space;
			}
			doc.setPreprocessedText(preprocessedText);
		}
		model.setPrefixes(prefixes);
		model.setSuffixes(suffixes);
		model.setWholeTokens(wholeTokens);
	}
	private void applySegmentation(){
		//if the segmentation is disabled then do just tokenization
		if (!configuration.getSegmentation()) {
			for (BinaryDocument document : corpus.getDocuments()) {
				document.setTokenizedText(tokenize(document.getText()));
				String preprocessedText="";
				for (String word : document.getTokenizedText()) {
					preprocessedText = preprocessedText+word+space;
				}
				document.setPreprocessedText(preprocessedText);
			}
			return;
		}
		for (BinaryDocument doc : corpus.getDocuments()) {
			doc.setTokenizedText(tokenize(doc.getText()));
			for (String token : doc.getTokenizedText()) {
				double biggestWeight = (model.getWholeTokens().containsKey(token))? Math.abs(model.getWholeTokens().get(token).getWeight()): 0.0;
				String segmentedToken = token;
				//generate the prefixes and suffixes
				for (int i = 1; i <= 4; i++) {
					//if the length of the token is smaller then the size of the prefix and suffix jump out
					if (token.length()<=i) {
						break;
					}
					String prefix = token.substring(0, i);
					if (model.getPrefixes().containsKey(prefix) && Math.abs(model.getPrefixes().get(prefix).getWeight())>biggestWeight) {
						biggestWeight = model.getPrefixes().get(prefix).getWeight();
						segmentedToken = prefix+space+token.substring(i);
					}
					String suffix = token.substring(token.length()-i);
					if (model.getSuffixes().containsKey(suffix) && Math.abs(model.getSuffixes().get(suffix).getWeight())>biggestWeight) {
						biggestWeight = model.getSuffixes().get(suffix).getWeight();
						segmentedToken = token.substring(0,token.length()-i)+space+suffix;
					}
				}
				doc.setPreprocessedText(doc.getPreprocessedText()+segmentedToken+space);
			}
		}
	}
	private void applyNGramGeneration(){
		for (BinaryDocument document : corpus.getDocuments()) {
			NGramGenerator nGramGenerator = new NGramGenerator(space, model.getCharacterNGrams(), configuration.getMaxNGramSize(), configuration.getNGramPatterns());
			document.setStringNGrams(nGramGenerator.getStringNGrams(document.getPreprocessedText()));
		}
	}
	private void classifyDocuments (){
		for (BinaryDocument doc : corpus.getDocuments()) {
			List<FeatureOccurrence> listOfIdentifiedFeatures = new ArrayList<>();
			for (StringNGram stringNGram : doc.getStringNGrams()) {
				//if the stringNGram is not in the model continue
				if (!model.getFeatures().containsKey(stringNGram.getnGram())) {
					continue;
				}
				//if the weight is positive but bellow the threshold continue
				if (model.getFeatures().get(stringNGram.getnGram()).getWeight()>0 && model.getFeatures().get(stringNGram.getnGram()).getWeight()<model.getThresholdWeightsPositive()) {
					continue;
				} 
				//if the weight is negative but above the threshold continue
				if (model.getFeatures().get(stringNGram.getnGram()).getWeight()<0 && model.getFeatures().get(stringNGram.getnGram()).getWeight()>model.getThresholdWeightsNegative()) {
					continue;
				}
				FeatureOccurrence featureOccurence = new FeatureOccurrence();
				featureOccurence.setWeight(model.getFeatures().get(stringNGram.getnGram()).getWeight());
				featureOccurence.setWords(stringNGram.getWords());
				listOfIdentifiedFeatures.add(featureOccurence);
			}
			getFeaturesSum(doc, listOfIdentifiedFeatures);
			Double probability = getProbability(model.getBeta0(), model.getBeta1(), model.getBeta2(), doc);
			if (probability>0.5) {
				doc.setConfidence(probability);
				if (model.getEstimatedAccuracy()>0.5) {
					doc.setModelBasedClass(1);
				} else {
					doc.setModelBasedClass(0);
				}
			} else {
				doc.setConfidence(1-probability);
				if (model.getEstimatedAccuracy()>0.5) {
					doc.setModelBasedClass(0);
				} else {
					doc.setModelBasedClass(1);
				}
			}
		}
	}
}
