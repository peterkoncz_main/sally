package sk.koncz.sally;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import sk.koncz.sally.datamodel.Feature;
import sk.koncz.sally.datamodel.StringNGram;

public class NGramGenerator {
	private static String specialEmptyString = "specialEmptyString";
	private static String specialConnectionString = " specialConnectionString ";
	private HashMap<String, Feature> relevantNGrams;
	private String space;
	private int maxNGramSize;
	private Boolean nGramPattern;
	private List<String> wordsFromDocument = new ArrayList<>();
	private Set<StringNGram> stringNGrams = new HashSet<StringNGram>();
	public NGramGenerator(String space, HashMap<String, Feature> relevantNGrams, int maxNGramSize, Boolean nGramPattern) {
		this.relevantNGrams = relevantNGrams;
		this.space = space;
		this.maxNGramSize = maxNGramSize;
		this.nGramPattern = nGramPattern;
	}
	public Set<StringNGram> getStringNGrams(String text) {
		String[] tokens = text.split(space);
		String bw1 = specialEmptyString;
		String bw2 = specialEmptyString;
		String bw3 = specialEmptyString;
		String bw4 = specialEmptyString;
		String composedWord = "";
		int stringNumber = 0; //the string order is used as a unique identifier of string, we need a unique ID of each word to able to identify the string n-grams which are contained in other n-grams
		for (String token : tokens) {
			stringNumber++;
			if (token==null) {
				break;
			}
			//1-gram
			List<Integer> words = Arrays.asList(stringNumber);
			//Arrays.asList(stringNumber); //contains the IDs of strings in the string n-gram
			stringNGrams.add(new StringNGram(token, words));
			wordsFromDocument.add(token);
			//2-gram
			if (!bw1.equals(specialEmptyString)&&maxNGramSize>1) {
				composedWord = bw1+specialConnectionString+token;
				addComposedWord(composedWord, Arrays.asList(stringNumber, stringNumber-1));
			}
			//3-gram
			if (!bw2.equals(specialEmptyString)&&maxNGramSize>2) {
				composedWord = bw2+specialConnectionString+bw1+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-1, stringNumber-2);
				addComposedWord(composedWord, words);
				if (nGramPattern) {
					composedWord = bw2+specialConnectionString+specialEmptyString+specialConnectionString+token;
					words = Arrays.asList(stringNumber, stringNumber-2);
					addComposedWord(composedWord, words);
				}
			}
			//4-gram
			if (!bw3.equals(specialEmptyString)&&maxNGramSize>3) {
				composedWord = bw3+specialConnectionString+bw2+specialConnectionString+bw1+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-1, stringNumber-2, stringNumber-3);
				addComposedWord(composedWord, words);
				if (nGramPattern) {
				composedWord = bw3+specialConnectionString+specialEmptyString+specialConnectionString+specialEmptyString+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-3);
				addComposedWord(composedWord, words);
				composedWord = bw3+specialConnectionString+bw2+specialConnectionString+specialEmptyString+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-2, stringNumber-3);
				addComposedWord(composedWord, words);
				composedWord = bw3+specialConnectionString+specialEmptyString+specialConnectionString+bw1+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-1, stringNumber-3);
				addComposedWord(composedWord, words);
				}
			}
			//5-gram
			if (!bw4.equals(specialEmptyString)&&maxNGramSize>4) {
				//*****
				composedWord = bw4+specialConnectionString+bw3+specialConnectionString+bw2+specialConnectionString+bw1+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-4, stringNumber-3, stringNumber-2, stringNumber-1);
				addComposedWord(composedWord, words);
				if (nGramPattern) {
				//*---*
				composedWord = bw4+specialConnectionString+specialEmptyString+specialConnectionString+specialEmptyString+specialConnectionString+specialEmptyString+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-4);
				addComposedWord(composedWord, words);
				//**--*
				composedWord = bw4+specialConnectionString+bw3+specialConnectionString+specialEmptyString+specialConnectionString+specialEmptyString+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-4, stringNumber-3);
				addComposedWord(composedWord, words);
				//***-*
				composedWord = bw4+specialConnectionString+bw3+specialConnectionString+bw2+specialConnectionString+specialEmptyString+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-4, stringNumber-3, stringNumber-2);
				addComposedWord(composedWord, words);
				//*--**
				composedWord = bw4+specialConnectionString+specialEmptyString+specialConnectionString+specialEmptyString+specialConnectionString+bw1+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-4, stringNumber-1);
				addComposedWord(composedWord, words);
				//*-***
				composedWord = bw4+specialConnectionString+specialEmptyString+specialConnectionString+bw2+specialConnectionString+bw1+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-4, stringNumber-2, stringNumber-1);
				addComposedWord(composedWord, words);
				//**-**
				composedWord = bw4+specialConnectionString+bw3+specialConnectionString+specialEmptyString+specialConnectionString+bw1+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-4, stringNumber-3, stringNumber-1);
				addComposedWord(composedWord, words);
				//*-*-*
				composedWord = bw4+specialConnectionString+specialEmptyString+specialConnectionString+bw2+specialConnectionString+specialEmptyString+specialConnectionString+token;
				words = Arrays.asList(stringNumber, stringNumber-4, stringNumber-2);
				addComposedWord(composedWord, words);
				}
			}
			bw4 = bw3;
			bw3 = bw2;
			bw2 = bw1;
			bw1 = token;
		}
		return stringNGrams;
	}
	public Set<String> getWords(List<String> filteredTokens){
		Set<String> wordsFromDocument = new HashSet<String>();
		String bw1 = specialEmptyString;
		String bw2 = specialEmptyString;
		String bw3 = specialEmptyString;
		String bw4 = specialEmptyString;

		Set<String> composedWords = new HashSet<String>();
		for (String token : filteredTokens) {
			if (token!=null) {
				//1-gram
				wordsFromDocument.add(token);
				//2-gram
				if (!bw1.equals(specialEmptyString)&&maxNGramSize>1) {
					composedWords.add(bw1+specialConnectionString+token);
				}
				//3-gram
				if (!bw2.equals(specialEmptyString)&&maxNGramSize>2) {
					composedWords.add(bw2+specialConnectionString+bw1+specialConnectionString+token);
					if (nGramPattern) {
						composedWords.add(bw2+specialConnectionString+specialEmptyString+specialConnectionString+token);
					}
				}
				//4-gram
				if (!bw3.equals(specialEmptyString)&&maxNGramSize>3) {
					composedWords.add(bw3+specialConnectionString+bw2+specialConnectionString+bw1+specialConnectionString+token);
					if (nGramPattern) {
						composedWords.add(bw3+specialConnectionString+specialEmptyString+specialConnectionString+specialEmptyString+specialConnectionString+token);
						composedWords.add(bw3+specialConnectionString+bw2+specialConnectionString+specialEmptyString+specialConnectionString+token);
						composedWords.add(bw3+specialConnectionString+specialEmptyString+specialConnectionString+bw1+specialConnectionString+token);
					}
				}
				//5-grams
				if (!bw4.equals(specialEmptyString)&&maxNGramSize>4) {
					//*****
					composedWords.add(bw4+specialConnectionString+bw3+specialConnectionString+bw2+specialConnectionString+bw1+specialConnectionString+token);
					if (nGramPattern) {
					//*---*
					composedWords.add(bw4+specialConnectionString+specialEmptyString+specialConnectionString+specialEmptyString+specialConnectionString+specialEmptyString+specialConnectionString+token);
					//**--*
					composedWords.add(bw4+specialConnectionString+bw3+specialConnectionString+specialEmptyString+specialConnectionString+specialEmptyString+specialConnectionString+token);
					//***-*
					composedWords.add(bw4+specialConnectionString+bw3+specialConnectionString+bw2+specialConnectionString+specialEmptyString+specialConnectionString+token);
					//*--**
					composedWords.add(bw4+specialConnectionString+specialEmptyString+specialConnectionString+specialEmptyString+specialConnectionString+bw1+specialConnectionString+token);
					//*-***
					composedWords.add(bw4+specialConnectionString+specialEmptyString+specialConnectionString+bw2+specialConnectionString+bw1+specialConnectionString+token);
					//**-**
					composedWords.add(bw4+specialConnectionString+bw3+specialConnectionString+specialEmptyString+specialConnectionString+bw1+specialConnectionString+token);
					//*-*-*
					composedWords.add(bw4+specialConnectionString+specialEmptyString+specialConnectionString+bw2+specialConnectionString+specialEmptyString+specialConnectionString+token);
					}
				}
				bw4 = bw3;
				bw3 = bw2;
				bw2 = bw1;
				bw1 = token;
			}
		}
		return composedWords;
	}
	private void addComposedWord(String composedWord, List<Integer> words) {
		if (relevantNGrams.containsKey(composedWord)) {
			stringNGrams.add(new StringNGram(composedWord, words));
			wordsFromDocument.add(composedWord);
		}
	}
}
