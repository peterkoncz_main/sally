package sk.koncz.sally.datamodel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Corpus {
	private List<Classifiable> documents = new ArrayList<>();
	public List<Classifiable> getDocuments() {
		return documents;
	}
	public void setDocuments(List<Classifiable> documents) {
		this.documents = documents;
	}
	public void addDocument(Document document) {
		documents.add(document);
	}
}
