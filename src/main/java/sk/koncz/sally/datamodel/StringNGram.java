package sk.koncz.sally.datamodel;

import java.util.ArrayList;
import java.util.List;

public class StringNGram {
	private String nGram = "";
	private List<Integer> words = new ArrayList<>();
	private List<Integer> descendants = new ArrayList<>();
	public String getnGram() {
		return nGram;
	}
	public void setnGram(String nGram) {
		this.nGram = nGram;
	}
	public List<Integer> getWords() {
		return words;
	}
	public void setWords(List<Integer> words) {
		this.words = words;
	}
	public List<Integer> getDescendants() {
		return descendants;
	}
	public void setDescendants(List<Integer> descendants) {
		this.descendants = descendants;
	}
	public StringNGram (String nGram, List<Integer> words){
		this.nGram=nGram;
		this.words=words;
	}
	public void addDescendant(int i) {
		this.descendants.add(i);
	}
}
