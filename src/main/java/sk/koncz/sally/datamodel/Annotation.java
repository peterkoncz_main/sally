package sk.koncz.sally.datamodel;

public class Annotation {
	private int from;
	private int to;
	private String text;
	
	public int getFrom() {
		return from;
	}
	
	public void setFrom(int from) {
		this.from = from;
	}
	
	public int getTo() {
		return to;
	}
	
	public void setTo(int to) {
		this.to = to;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public Annotation(int from, int to, String text) {
		this.from = from;
		this.to = to;
		this.text = text;
	}

}
