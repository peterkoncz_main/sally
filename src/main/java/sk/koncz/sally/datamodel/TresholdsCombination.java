package sk.koncz.sally.datamodel;

public class TresholdsCombination {
	private Double positiveTreshold = 0.0;
	private Double negativeTreshold = 0.0;
	private Double separatingTreshold = 0.0;
	private int featuresNum;
	public Double getPositiveTreshold() {
		return positiveTreshold;
	}
	public void setPositiveTreshold(Double positiveTreshold) {
		this.positiveTreshold = positiveTreshold;
	}
	public Double getNegativeTreshold() {
		return negativeTreshold;
	}
	public void setNegativeTreshold(Double negativeTreshold) {
		this.negativeTreshold = negativeTreshold;
	}
	public Double getSeparatingTreshold() {
		return separatingTreshold;
	}
	public void setSeparatingTreshold(Double separatingTreshold) {
		this.separatingTreshold = separatingTreshold;
	}
	public int getFeaturesNum() {
		return featuresNum;
	}
	public void setFeaturesNum(int featuresNum) {
		this.featuresNum = featuresNum;
	}
}
