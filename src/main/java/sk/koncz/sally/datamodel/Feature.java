package sk.koncz.sally.datamodel;

public class Feature implements java.io.Serializable{
	private int occurenceClassA = 0;
	private int occurenceClassB = 0;
	private Double weight;
	
	public int getOccurenceClassA() {
		return occurenceClassA;
	}
	public void setOccurenceClassA(int occurenceClassA) {
		this.occurenceClassA = occurenceClassA;
	}
	public int getOccurenceClassB() {
		return occurenceClassB;
	}
	public void setOccurenceClassB(int occurenceClassB) {
		this.occurenceClassB = occurenceClassB;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	
	
}
