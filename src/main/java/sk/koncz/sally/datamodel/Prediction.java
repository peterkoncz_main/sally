package sk.koncz.sally.datamodel;

public class Prediction {
	private int label;
	private double weight;
	private int classPrediction;
	private double confidence;
	private int docClass;
	public Prediction(int label, double weight, int classPrediction, double confidence, int docClass ){
		this.setLabel(label);
		this.setWeight(weight);
		this.setClassPrediction(classPrediction);
		this.setConfidence(confidence);
		this.docClass=docClass;
	}
	public int getLabel() {
		return label;
	}
	public void setLabel(int label) {
		this.label = label;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public int getClassPrediction() {
		return classPrediction;
	}
	public void setClassPrediction(int classPrediction) {
		this.classPrediction = classPrediction;
	}
	public double getConfidence() {
		return confidence;
	}
	public void setConfidence(double confidence) {
		this.confidence = confidence;
	}
}
