package sk.koncz.sally.datamodel;

import java.util.ArrayList;
import java.util.List;

public class BinaryCorpus {
	private List<BinaryDocument> documents = new ArrayList<BinaryDocument>();
	private int numDocuments;
	private int numPositiveDocuments;
	private int numNegativeDocuments;
	private Double recallPositive;
	private Double recallNegative;
	private Double precisionPositive;
	private Double precisionNegative;
	private Double accuracy;
	private Double estimatedAccuracy;
	private int truePositive;
	private int trueNegative;
	private int falsePositive;
	private int falseNegative;
	private Double truePositiveWeight;
	private Double trueNegativeWeight;
	private Double falseNegativeWeight;
	private Double falsePositiveWeight;
	private Integer label;

	public List<BinaryDocument> getDocuments() {
		return documents;
	}
	public void setDocuments(List<BinaryDocument> documents) {
		this.documents = documents;
	}
	public int getNumDocuments() {
		return numDocuments;
	}
	public void setNumDocuments(int numDocuments) {
		this.numDocuments = numDocuments;
	}
	public int getNumPositiveDocuments() {
		return numPositiveDocuments;
	}
	public void setNumPositiveDocuments(int numPositiveDocuments) {
		this.numPositiveDocuments = numPositiveDocuments;
	}
	public int getNumNegativeDocuments() {
		return numNegativeDocuments;
	}
	public void setNumNegativeDocuments(int numNegativeDocuments) {
		this.numNegativeDocuments = numNegativeDocuments;
	}
	public void addDocument(BinaryDocument document) {
		documents.add(document);
	}
	public Double getRecallPositive() {
		return recallPositive;
	}
	public void setRecallPositive(Double recallPositive) {
		this.recallPositive = recallPositive;
	}
	public Double getRecallNegative() {
		return recallNegative;
	}
	public void setRecallNegative(Double recallNegative) {
		this.recallNegative = recallNegative;
	}
	public Double getPrecisionPositive() {
		return precisionPositive;
	}
	public void setPrecisionPositive(Double precisionPositive) {
		this.precisionPositive = precisionPositive;
	}
	public Double getPrecisionNegative() {
		return precisionNegative;
	}
	public void setPrecisionNegative(Double precisionNegative) {
		this.precisionNegative = precisionNegative;
	}
	public Double getAccuracy() {
		return accuracy;
	}
	public void setAccuracy(Double accuracy) {
		this.accuracy = accuracy;
	}
	public int getTruePositive() {
		return truePositive;
	}
	public void setTruePositive(int truePositive) {
		this.truePositive = truePositive;
	}
	public int getTrueNegative() {
		return trueNegative;
	}
	public void setTrueNegative(int trueNegative) {
		this.trueNegative = trueNegative;
	}
	public int getFalsePositive() {
		return falsePositive;
	}
	public void setFalsePositive(int falsePositive) {
		this.falsePositive = falsePositive;
	}
	public int getFalseNegative() {
		return falseNegative;
	}
	public void setFalseNegative(int falseNegative) {
		this.falseNegative = falseNegative;
	}
	public Double getEstimatedAccuracy() {
		return estimatedAccuracy;
	}
	public void setEstimatedAccuracy(Double estimatedAccuracy) {
		this.estimatedAccuracy = estimatedAccuracy;
	}
	public Double getTruePositiveWeight() {
		return truePositiveWeight;
	}
	public void setTruePositiveWeight(Double truePositiveWeight) {
		this.truePositiveWeight = truePositiveWeight;
	}
	public Double getTrueNegativeWeight() {
		return trueNegativeWeight;
	}
	public void setTrueNegativeWeight(Double trueNegativeWeight) {
		this.trueNegativeWeight = trueNegativeWeight;
	}
	public Double getFalseNegativeWeight() {
		return falseNegativeWeight;
	}
	public void setFalseNegativeWeight(Double falseNegativeWeight) {
		this.falseNegativeWeight = falseNegativeWeight;
	}
	public Double getFalsePositiveWeight() {
		return falsePositiveWeight;
	}
	public void setFalsePositiveWeight(Double falsePositiveWeight) {
		this.falsePositiveWeight = falsePositiveWeight;
	}
	public Integer getLabel() {
		return label;
	}
	public void setLabel(Integer label) {
		this.label = label;
	}
}
