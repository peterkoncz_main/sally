package sk.koncz.sally.datamodel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class BinaryDocument {
	private String id; 
	private String text;
	private String[] tokenizedText;
	private String preprocessedText = "";
	private Set<StringNGram> stringNGrams = new HashSet<StringNGram>();
	private int docClass;
	private int modelBasedClass;
	private HashMap<Integer, Double> modelBasedClases;
	private Double confidence;
	private List<Annotation> annotations;
	private HashMap<String, Feature> features;
	private HashMap<Integer, Double> wordWeights;
	private String highlightedText;
	private Double featuresSum;
	private Double positiveFeaturesSum;
	private Double negativeFeaturesSum;
	private int usedFeatures, usedPositiveFeatures, usedNegativeFeatures;
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getDocClass() {
		return docClass;
	}
	public void setDocClass(int docClass) {
		this.docClass = docClass;
	}
	public Double getConfidence() {
		return confidence;
	}
	public void setConfidence(Double confidence) {
		this.confidence = confidence;
	}
	public List<Annotation> getAnnotations() {
		return annotations;
	}
	public void addAnnotation(Annotation annotation) {
		this.annotations.add(annotation);
	}
	public BinaryDocument(String text) {
		this.text=text;
		this.id = UUID.randomUUID().toString();
	}
	public String getPreprocessedText() {
		return preprocessedText;
	}
	public void setPreprocessedText(String preprocessedText) {
		this.preprocessedText = preprocessedText;
	}
	public HashMap<String, Feature> getFeatures() {
		return features;
	}
	public void setFeatures(HashMap<String, Feature> features) {
		this.features = features;
	}
	public Double getFeaturesSum() {
		return featuresSum;
	}
	public void setFeaturesSum(Double featuresSum) {
		this.featuresSum = featuresSum;
	}
	public int getModelBasedClass() {
		return modelBasedClass;
	}
	public void setModelBasedClass(int modelBasedClass) {
		this.modelBasedClass = modelBasedClass;
	}
	public Set<StringNGram> getStringNGrams() {
		return stringNGrams;
	}
	public void setStringNGrams(Set<StringNGram> stringNGrams) {
		this.stringNGrams = stringNGrams;
	}
	public String[] getTokenizedText() {
		return tokenizedText;
	}
	public void setTokenizedText(String[] tokenizedText) {
		this.tokenizedText = tokenizedText;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id=id;
	}
	public HashMap<Integer, Double> getWordWeights() {
		return wordWeights;
	}
	public void setWordWeights(HashMap<Integer, Double> wordWeights) {
		this.wordWeights = wordWeights;
	}
	public String getHighlightedText() {
		return highlightedText;
	}
	public void setHighlightedText(String highlightedText) {
		this.highlightedText = highlightedText;
	}
	public Double getPositiveFeaturesSum() {
		return positiveFeaturesSum;
	}
	public void setPositiveFeaturesSum(Double positiveFeaturesSum) {
		this.positiveFeaturesSum = positiveFeaturesSum;
	}
	public Double getNegativeFeaturesSum() {
		return negativeFeaturesSum;
	}
	public void setNegativeFeaturesSum(Double negativeFeaturesSum) {
		this.negativeFeaturesSum = negativeFeaturesSum;
	}
	public HashMap<Integer, Double> getModelBasedClases() {
		return modelBasedClases;
	}
	public void setModelBasedClases(HashMap<Integer, Double> modelBasedClases) {
		this.modelBasedClases = modelBasedClases;
	}
	public int getUsedFeatures() {
		return usedFeatures;
	}
	public void setUsedFeatures(int usedFeatures) {
		this.usedFeatures = usedFeatures;
	}
	public int getUsedPositiveFeatures() {
		return usedPositiveFeatures;
	}
	public void setUsedPositiveFeatures(int usedPositiveFeatures) {
		this.usedPositiveFeatures = usedPositiveFeatures;
	}
	public int getUsedNegativeFeatures() {
		return usedNegativeFeatures;
	}
	public void setUsedNegativeFeatures(int usedNegativeFeatures) {
		this.usedNegativeFeatures = usedNegativeFeatures;
	}
}
