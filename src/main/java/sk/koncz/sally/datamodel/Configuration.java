package sk.koncz.sally.datamodel;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Generated("org.jsonschema2pojo")
public class Configuration implements Serializable{

@SerializedName("description")
@Expose
private String description;
@SerializedName("weightingMethod")
@Expose
private String weightingMethod="OWN";
@SerializedName("segmentation")
@Expose
private Boolean segmentation=false;
@SerializedName("numTopPrefixes")
@Expose
private Integer numTopPrefixes=20;
@SerializedName("numTopSuffixes")
@Expose
private Integer numTopSuffixes=20;
@SerializedName("maxNGramSize")
@Expose
private Integer maxNGramSize=3;
@SerializedName("nGramPatterns")
@Expose
private Boolean nGramPatterns=false;
@SerializedName("filterOverliedNGrams")
@Expose
private Boolean filterOverliedNGrams=true;
@SerializedName("solver")
@Expose
private String solver="L1R_LR";
@SerializedName("C")
@Expose
private Integer C=1;// cost of constraints violation
@SerializedName("eps")
@Expose
private Double eps=0.1;// stopping criteria
@SerializedName("weightsTresholdOptimization")
@Expose
private Boolean weightsTresholdOptimization=false;
@SerializedName("maxDowngradeIterations")
@Expose
private Integer maxDowngradeIterations=100000;
@SerializedName("maxWeightThreshold")
@Expose
private Double maxWeightThreshold=0.8;
@SerializedName("minWeightThreshold")
@Expose
private Double minWeightThreshold=-0.8;
@SerializedName("accuracyTollerance")
@Expose
private Double accuracyTollerance=0.01;
@SerializedName("highlightText")
@Expose
private Boolean highlightText=false;
@SerializedName("sampling")
@Expose
private String sampling = "subsampling";
/**
* 
* @return
* The description
*/
public String getDescription() {
return description;
}

/**
* 
* @param description
* The description
*/
public void setDescription(String description) {
this.description = description;
}

/**
* 
* @return
* The weightingMethod
*/
public String getWeightingMethod() {
return weightingMethod;
}

/**
* 
* @param weightingMethod
* The weightingMethod
*/
public void setWeightingMethod(String weightingMethod) throws Exception {
    if (!(weightingMethod.equals("OWN")||weightingMethod.equals("IG"))) {
        throw new Exception("The method must be OWN or IG!");
    }
    this.weightingMethod = weightingMethod;
}

/**
* 
* @return
* The segmentation
*/
public Boolean getSegmentation() {
return segmentation;
}

/**
* 
* @param segmentation
* The segmentation
*/
public void setSegmentation(Boolean segmentation) {
this.segmentation = segmentation;
}

/**
* 
* @return
* The numTopPrefixes
*/
public Integer getNumTopPrefixes() {
return numTopPrefixes;
}

/**
* 
* @param numTopPrefixes
* The numTopPrefixes
*/
public void setNumTopPrefixes(Integer numTopPrefixes) {
this.numTopPrefixes = numTopPrefixes;
}

/**
* 
* @return
* The numTopSuffixes
*/
public Integer getNumTopSuffixes() {
return numTopSuffixes;
}

/**
* 
* @param numTopSuffixes
* The numTopSuffixes
*/
public void setNumTopSuffixes(Integer numTopSuffixes) {
this.numTopSuffixes = numTopSuffixes;
}

/**
* 
* @return
* The maxNGramSize
*/
public Integer getMaxNGramSize() {
return maxNGramSize;
}

/**
* 
* @param maxNGramSize
* The maxNGramSize
*/
public void setMaxNGramSize(Integer maxNGramSize) {
    if (!(maxNGramSize>0&&maxNGramSize<6)) {
        throw new ArithmeticException("The maxNGramSize should be from interval <1;5>!");
    }
    this.maxNGramSize = maxNGramSize;
}

/**
* 
* @return
* The nGramPatterns
*/
public Boolean getNGramPatterns() {
return nGramPatterns;
}

/**
* 
* @param nGramPatterns
* The nGramPatterns
*/
public void setNGramPatterns(Boolean nGramPatterns) {
this.nGramPatterns = nGramPatterns;
}

/**
* 
* @return
* The filterOverliedNGrams
*/
public Boolean getFilterOverliedNGrams() {
return filterOverliedNGrams;
}

/**
* 
* @param filterOverliedNGrams
* The filterOverliedNGrams
*/
public void setFilterOverliedNGrams(Boolean filterOverliedNGrams) {
this.filterOverliedNGrams = filterOverliedNGrams;
}

/**
* 
* @return
* The solver
*/
public String getSolver() {
return solver;
}

/**
* 
* @param solver
* The solver
*/
public void setSolver(String solver) throws Exception {
    if (!(solver.equals("L1R_LR")||solver.equals("L2R_LR"))) {
        throw new Exception("The solver must be L1R_LR or L2R_LR!");
    }
    this.solver = solver;
}

/**
* 
* @return
* The C
*/
public Integer getC() {
return C;
}

/**
* 
* @param C
* The C
*/
public void setC(Integer C) {
this.C = C;
}

/**
* 
* @return
* The eps
*/
public Double getEps() {
return eps;
}

/**
* 
* @param eps
* The eps
*/
public void setEps(Double eps) {
this.eps = eps;
}

/**
* 
* @return
* The weightsTresholdOptimization
*/
public Boolean getWeightsTresholdOptimization() {
return weightsTresholdOptimization;
}

/**
* 
* @param weightsTresholdOptimization
* The weightsTresholdOptimization
*/
public void setWeightsTresholdOptimization(Boolean weightsTresholdOptimization) {
this.weightsTresholdOptimization = weightsTresholdOptimization;
}

/**
* 
* @return
* The maxDowngradeIterations
*/
public Integer getMaxDowngradeIterations() {
return maxDowngradeIterations;
}

/**
* 
* @param maxDowngradeIterations
* The maxDowngradeIterations
*/
public void setMaxDowngradeIterations(Integer maxDowngradeIterations) {
this.maxDowngradeIterations = maxDowngradeIterations;
}

/**
* 
* @return
* The maxWeightThreshold
*/
public Double getMaxWeightThreshold() {
return maxWeightThreshold;
}

/**
* 
* @param maxWeightThreshold
* The maxWeightThreshold
*/
public void setMaxWeightThreshold(Double maxWeightThreshold) {
    if ((maxWeightThreshold<0||maxWeightThreshold>1)) {
        throw new ArithmeticException("The maxWeightThreshold should be from interval <0;1>!");
    }
    this.maxWeightThreshold = maxWeightThreshold;
}

/**
* 
* @return
* The minWeightThreshold
*/
public Double getMinWeightThreshold() {
return minWeightThreshold;
}

/**
* 
* @param minWeightThreshold
* The minWeightThreshold
*/
public void setMinWeightThreshold(Double minWeightThreshold) {
    if ((minWeightThreshold>0||minWeightThreshold<-1)) {
        throw new ArithmeticException("The minWeightThreshold should be from interval <-1;0>!");
    }
    this.minWeightThreshold = minWeightThreshold;
}

/**
* 
* @return
* The accuracyTollerance
*/
public Double getAccuracyTollerance() {
return accuracyTollerance;
}

/**
* 
* @param accuracyTollerance
* The accuracyTollerance
*/
public void setAccuracyTollerance(Double accuracyTollerance) {
this.accuracyTollerance = accuracyTollerance;
}

/**
* 
* @return
* The highlightText
*/
public Boolean getHighlightText() {
return highlightText;
}

/**
* 
* @param highlightText
* The highlightText
*/
public void setHighlightText(Boolean highlightText) {
this.highlightText = highlightText;
}

/**
* 
* @return
* The sampling
*/
public String getSampling() {
return sampling;
}

/**
* 
* @param sampling
* The sampling
*/
public void setSampling(String sampling) {
this.sampling = sampling;
}

}