package sk.koncz.sally.datamodel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class BinaryModel implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2602643734856226222L;
	private HashMap<String, Feature> characterNGrams = new HashMap<>();
	private HashMap<String, Feature> prefixes = new HashMap<>();
	private HashMap<String, Feature> suffixes = new HashMap<>();
	private HashMap<String, Feature> wholeTokens = new HashMap<>();
	private HashMap<String, Feature> features = new HashMap<>();
	private Double thresholdWeightsPositive;
	private Double thresholdWeightsNegative;
	private Double beta0;
	private Double beta1;
	private Double beta2;
	private Double threshold;
	private Double estimatedAccuracy;
	private Integer label;
	private Configuration configuration;
	
	public HashMap<String, Feature> getFeatures() {
		return features;
	}
	public void setFeatures(HashMap<String, Feature> features) {
		this.features = features;
	}
	public Double getThreshold() {
		return threshold;
	}
	public void setThreshold(Double threshold) {
		this.threshold = threshold;
	}
	public BinaryModel() {
		
	}
	public void save(String path) {
		try {
			File file = new File(path);
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.close();

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public BinaryModel(File model) {
		
	}
	public HashMap<String, Feature> getCharacterNGrams() {
		return characterNGrams;
	}
	public void setCharacterNGrams(HashMap<String, Feature> characterNGrams) {
		this.characterNGrams = characterNGrams;
	}
	public Double getThresholdWeightsPositive() {
		return thresholdWeightsPositive;
	}
	public void setThresholdWeightsPositive(Double thresholdWeightsPositive) {
		this.thresholdWeightsPositive = thresholdWeightsPositive;
	}
	public Double getThresholdWeightsNegative() {
		return thresholdWeightsNegative;
	}
	public void setThresholdWeightsNegative(Double thresholdWeightsNegative) {
		this.thresholdWeightsNegative = thresholdWeightsNegative;
	}
	public Double getEstimatedAccuracy() {
		return estimatedAccuracy;
	}
	public void setEstimatedAccuracy(Double estimatedAccuracy) {
		this.estimatedAccuracy = estimatedAccuracy;
	}
	public HashMap<String, Feature> getPrefixes() {
		return prefixes;
	}
	public void setPrefixes(HashMap<String, Feature> prefixes) {
		this.prefixes = prefixes;
	}
	public HashMap<String, Feature> getSuffixes() {
		return suffixes;
	}
	public void setSuffixes(HashMap<String, Feature> suffixes) {
		this.suffixes = suffixes;
	}
	public HashMap<String, Feature> getWholeTokens() {
		return wholeTokens;
	}
	public void setWholeTokens(HashMap<String, Feature> wholeTokens) {
		this.wholeTokens = wholeTokens;
	}
	public Double getBeta0() {
		return beta0;
	}
	public void setBeta0(Double beta0) {
		this.beta0 = beta0;
	}
	public Double getBeta1() {
		return beta1;
	}
	public void setBeta1(Double beta1) {
		this.beta1 = beta1;
	}
	public Double getBeta2() {
		return beta2;
	}
	public void setBeta2(Double beta2) {
		this.beta2 = beta2;
	}
	public Integer getLabel() {
		return label;
	}
	public void setLabel(Integer label) {
		this.label = label;
	}
	public Configuration getConfiguration() {
		return configuration;
	}
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
}
