package sk.koncz.sally.datamodel;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by Peter.Koncz on 7. 8. 2016.
 */
public interface Classifiable {
    String getId();
    String getText();
    HashMap<Integer, Double> getModelBasedClases();
    Set<Integer> getDocClasses();
    void setModelBasedClases(HashMap<Integer, Double> modelBasedClases);
    void setDocClasses(Set<Integer> docClasses);
    void setDocClass(Integer docClass);
    Set<Integer> getModelBasedTrueClases();
}
