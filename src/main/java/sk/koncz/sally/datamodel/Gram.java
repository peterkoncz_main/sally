package sk.koncz.sally.datamodel;

public class Gram {
	private String string;
	private String position;
	public String getString() {
		return string;
	}
	public void setString(String string) {
		this.string = string;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	} 
	public Gram(String string, String position) {
		setString(string);
		setPosition(position);
	}
}
