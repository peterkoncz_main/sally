package sk.koncz.sally.datamodel;

public class Statistics {
	private int targetClassSize, remainingClassesSize;

	public int getTargetClassSize() {
		return targetClassSize;
	}

	public void setTargetClassSize(int targetClassSize) {
		this.targetClassSize = targetClassSize;
	}

	public int getRemainingClassesSize() {
		return remainingClassesSize;
	}

	public void setRemainingClassesSize(int remainingClassesSize) {
		this.remainingClassesSize = remainingClassesSize;
	}
	
}
