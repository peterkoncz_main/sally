package sk.koncz.sally.datamodel;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Model extends BinaryModel implements Serializable{
	private static final long serialVersionUID = -6340143103030919878L;
	private List<BinaryModel> multiLabeledModels = new ArrayList<>();
	private HashMap<Integer, Integer> corporaStatistics;
	private Configuration configuration;

	public Model(List<BinaryModel> multiLabeledModels, HashMap<Integer, Integer> corporaStatistics, Configuration configuration){
		this.multiLabeledModels=multiLabeledModels;
		this.corporaStatistics=corporaStatistics;
		this.configuration=configuration;
	}
	public List<BinaryModel> getMultiLabeledModels() {
		return multiLabeledModels;
	}

	public void setMultiLabeledModels(List<BinaryModel> multiLabeledModels) {
		this.multiLabeledModels = multiLabeledModels;
	}

	public HashMap<Integer, Integer> getCorporaStatistics() {
		return corporaStatistics;
	}

	public void setCorporaStatistics(HashMap<Integer, Integer> corporaStatistics) {
		this.corporaStatistics = corporaStatistics;
	}

	@Override
	public Configuration getConfiguration() {
		return configuration;
	}

	@Override
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}
}
