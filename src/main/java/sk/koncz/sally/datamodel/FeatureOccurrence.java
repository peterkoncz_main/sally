package sk.koncz.sally.datamodel;

import java.util.ArrayList;
import java.util.List;

public class FeatureOccurrence {
	private Double weight;
	private List<Integer> words = new ArrayList<>(); 
	
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public List<Integer> getWords() {
		return words;
	}
	public void setWords(List<Integer> words) {
		this.words = words;
	}
	
	
}
