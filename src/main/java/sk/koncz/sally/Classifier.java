package sk.koncz.sally;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import sk.koncz.sally.datamodel.*;
import sk.koncz.sally.datamodel.BinaryCorpus;

public class Classifier{
	private Corpus originalCorpus;
	private List<BinaryCorpus> multiLabeledCorpora = new ArrayList<>();
	private List<BinaryModel> multiLabeledModels = new ArrayList<>();
	private HashMap<String, List<Prediction>> predictions;
	private HashMap<Integer, Integer> corporaStatistics;
	private Configuration configuration;
	
	/**
	 * Constructor of the Classifier
	 * @param configuration
	 * @throws Exception
	 */
	public Classifier(Configuration configuration) throws Exception {
        this.configuration=configuration;
	}
	/**
	 * Constructor of the Classifier
	 * @throws Exception
	 */
	public Classifier() throws Exception {
        this.configuration=new Configuration();
	}
	public Classifier(String path) throws Exception {
		this.configuration=new Configuration();
	}
	/**
	 * Method build models
	 * @param corpus
	 * @return
	 * @throws Exception
	 */
	public void train(Corpus corpus) throws Exception {
		this.originalCorpus = corpus;
		getCorporaStatistics();
		generateLabeledCorporaReSampling();
		this.multiLabeledModels = trainModels();
		this.originalCorpus = null;
		this.multiLabeledCorpora = null;
	}
	public void train(Corpus corpus, String path) throws Exception {
		train(corpus);
		saveModel(path);
	}
	public List<List<BinaryModel>>  buildMetaModel(Corpus corpus) throws Exception {
		this.originalCorpus = corpus;
		List<List<BinaryModel>> allModels = new ArrayList<List<BinaryModel>>();
		getCorporaStatistics();
		generateLabeledCorporaReSampling();
		allModels.add(trainModels());
		addClassAttributes();
		allModels.add(trainModels());
		return allModels;
	}
	/**
	 * Method to apply models
	 * @param corpus
	 * @throws Exception
	 */
	public void apply(Corpus corpus) throws Exception {
		this.originalCorpus = corpus;
		getPredictions();
		mergePredictions();
	}
	public void apply(Corpus corpus, String path) throws Exception {
		loadModel(path);
		this.originalCorpus = corpus;
		getPredictions();
		mergePredictions();
	}
	public void applyMetaModel(Corpus corpus, List<List<BinaryModel>> models) throws Exception {
		this.originalCorpus = corpus;
		this.multiLabeledModels = models.get(0);
		getPredictions();
		mergePredictions();
		addClassAttributes();
		this.multiLabeledModels = models.get(1);
		getPredictions();
		mergePredictions();
	}
	/**
	 * Method to save models
	 * @param path
	 */
	public void saveModel(String path){
		Model model = new Model(this.multiLabeledModels, this.corporaStatistics, this.configuration);
		try
		{
			FileOutputStream fileOut = new FileOutputStream(path);
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(model);
			out.close();
			fileOut.close();
		}catch(IOException i)
		{
			i.printStackTrace();
		}
	}
	/**
	 * Method to load saved models
	 * @param path
	 * @return
	 */
	public void loadModel(String path) throws ClassNotFoundException, IOException {
		FileInputStream fileIn = new FileInputStream(path);
		ObjectInputStream in = new ObjectInputStream(fileIn);
		Model model = (Model) in.readObject();
		in.close();
		fileIn.close();
		this.multiLabeledModels=model.getMultiLabeledModels();
		this.corporaStatistics=model.getCorporaStatistics();
		this.configuration=model.getConfiguration();
	}
	private void addClassAttributes() {
		/*
		//get the list of classes for each document
		HashMap<String, List<StringNGram>> documentClasses = new HashMap<String, List<StringNGram>>();
		for (BinaryCorpus corpus : multiLabeledCorpora) {
			for (BinaryDocument document : corpus.getDocuments()) {
				if (document.getModelBasedClass()==1) {
					List<StringNGram> currentList = (documentClasses.containsKey(document.getId()))? documentClasses.get(document.getId()): new ArrayList<StringNGram>();
					currentList.add(new StringNGram("specialClassNGram"+corpus.getLabel(), new ArrayList<Integer>(-1)));
					documentClasses.put(document.getId(), currentList);
				}
			}
		}
		//add the labels into the ngrams
		for (BinaryCorpus corpus : multiLabeledCorpora) {
			for (BinaryDocument document : corpus.getDocuments()) {
				if (documentClasses.containsKey(document.getId())) {
					document.getStringNGrams().addAll(documentClasses.get(document.getId()));
				}
			}
		}
		*/
		HashMap<String, String> documentClasses = new HashMap<String, String>();
		for (BinaryCorpus corpus : multiLabeledCorpora) {
			for (BinaryDocument document : corpus.getDocuments()) {
				if (document.getModelBasedClass()==1) {
					String currentString = (documentClasses.containsKey(document.getId()))? documentClasses.get(document.getId()): "";
					currentString = currentString+" "+corpus.getLabel()+"ClassNGram";
					documentClasses.put(document.getId(), currentString);
				}
			}
		}
		for (BinaryCorpus corpus : multiLabeledCorpora) {
			for (BinaryDocument document : corpus.getDocuments()) {
				if (documentClasses.containsKey(document.getId())) {
					document.setText(document.getText()+documentClasses.get(document.getId()));
				}
			}
		}
	}
	/**
	 * Get the numbers of occurrences for each label
	 * @throws Exception
	 */
	private void getCorporaStatistics() throws Exception{
		HashMap<Integer, Integer> corporaStatistics = new HashMap<>();
		for (Classifiable doc : originalCorpus.getDocuments()) {
			for (Integer docClass : doc.getDocClasses()) {
				corporaStatistics.put(docClass, corporaStatistics.containsKey(docClass)?corporaStatistics.get(docClass)+1:1);
			}
		}
		for (int label : corporaStatistics.keySet()) {
			if (corporaStatistics.get(label)<10) {
				throw new Exception("Each class should contain at least 10 examples!"); 
			}
		}
		this.corporaStatistics=corporaStatistics;
	}
	/**
	 * This method generates multiple binominal corpora 
	 */
	private void generateLabeledCorporaReSampling() {
		for (Integer label : corporaStatistics.keySet()) {
			int size = corporaStatistics.get(label);
			int sizeOfRemainingClasses = originalCorpus.getDocuments().size()-size;
			double ratio = (size>sizeOfRemainingClasses)? size/sizeOfRemainingClasses : sizeOfRemainingClasses/size;
			int biggerClass = (size>sizeOfRemainingClasses)? 1 : 0;
			List<BinaryDocument> documentsOfSmallerClass = new ArrayList<BinaryDocument>();
			List<BinaryDocument> documentsOfBiggerClass = new ArrayList<BinaryDocument>();
			//We create two groups of documents on for the smaller class and one for the bigger
			for (Classifiable multiLabelDocument : originalCorpus.getDocuments()) {
				BinaryDocument document = new BinaryDocument(multiLabelDocument.getText());
				document.setId(multiLabelDocument.getId());
				document.setDocClass((multiLabelDocument.getDocClasses().contains(label))? 1:0);
				if (biggerClass == document.getDocClass()) {
					documentsOfBiggerClass.add(document);
				}
				else {
					documentsOfSmallerClass.add(document);
				}
			}
			//for each label we will generate Math.ceil(ratio) corpora
			for (int i = 0; i < Math.ceil(ratio); i++) {
				BinaryCorpus corpus = new BinaryCorpus();
				corpus.setLabel(label);
				List<BinaryDocument> corpusDocuments = new ArrayList<BinaryDocument>();
				corpusDocuments.addAll(documentsOfSmallerClass);
				int lastIndex = (documentsOfBiggerClass.size()<(i+1)*documentsOfSmallerClass.size())? documentsOfBiggerClass.size():(i+1)*documentsOfSmallerClass.size();
				corpusDocuments.addAll(documentsOfBiggerClass.subList(i*documentsOfSmallerClass.size(), lastIndex));
				//adding already used documents of the majority class if needed 
				if (corpusDocuments.size()<documentsOfSmallerClass.size()*2) {
					corpusDocuments.addAll(documentsOfBiggerClass.subList(0, documentsOfSmallerClass.size()*2-corpusDocuments.size()));
				} 
				corpus.setDocuments(corpusDocuments);
				multiLabeledCorpora.add(corpus);
			}
			//this is a binary classification task we don't need to create the same corpora twice
			if (corporaStatistics.keySet().size()==2) {
				break;
			}
		}
	}
	private void generateLabeledCorporaSubSampling() {
		for (Integer label : corporaStatistics.keySet()) {
			int size = corporaStatistics.get(label);
			int sizeOfRemainingClasses = originalCorpus.getDocuments().size()-size;
			int biggerClass = (size>sizeOfRemainingClasses)? 1 : 0;
			List<BinaryDocument> documentsOfSmallerClass = new ArrayList<BinaryDocument>();
			List<BinaryDocument> documentsOfBiggerClass = new ArrayList<BinaryDocument>();
			//We create two groups of documents on for the smaller class and one for the bigger
			for (Classifiable multiLabelDocument : originalCorpus.getDocuments()) {
				BinaryDocument document = new BinaryDocument(multiLabelDocument.getText());
				document.setId(multiLabelDocument.getId());
				document.setDocClass((multiLabelDocument.getDocClasses().contains(label))? 1:0);
				if (biggerClass == document.getDocClass()) {
					documentsOfBiggerClass.add(document);
				}
				else {
					documentsOfSmallerClass.add(document);
				}
			}
			BinaryCorpus corpus = new BinaryCorpus();
			corpus.setLabel(label);
			List<BinaryDocument> corpusDocuments = new ArrayList<BinaryDocument>();
			corpusDocuments.addAll(documentsOfSmallerClass);
			corpusDocuments.addAll(documentsOfBiggerClass.subList(0, documentsOfSmallerClass.size()));
			corpus.setDocuments(corpusDocuments);
			multiLabeledCorpora.add(corpus);
			//this is a binary classification task we don't need to create the same corpora twice
			if (corporaStatistics.keySet().size()==2) {
				break;
			}
		}
	}
	/**
	 * Train the models using all the previously created corpora
	 * @throws Exception
	 */
	private List<BinaryModel> trainModels() throws Exception{
		List<BinaryModel> models = new ArrayList<>();
		for (BinaryCorpus corpus : multiLabeledCorpora) {
			BinaryClassifier classifier = new BinaryClassifier(configuration);
			BinaryModel model = classifier.buildModel(corpus);
			model.setLabel(corpus.getLabel());
			models.add(model);
		}
		return models;
	}
	/**
	 * Get the predictions from each previously created model for each document
	 * @throws Exception
	 */
	private void getPredictions() throws Exception{
		HashMap<String, List<Prediction>> predictions = new HashMap<>();
		//get the predictions using all the models
		for (BinaryModel model : multiLabeledModels) {
			BinaryClassifier classifier = new BinaryClassifier(configuration);
			BinaryCorpus currentCorpus = new BinaryCorpus();
			for (Classifiable multiLabelDocument : originalCorpus.getDocuments()) {
				BinaryDocument document = new BinaryDocument(multiLabelDocument.getText());
				document.setId(multiLabelDocument.getId());
				currentCorpus.addDocument(document);
			}
			currentCorpus = classifier.applyModel(currentCorpus, model);
			//add the prediction to predictions for all the documents
			for (BinaryDocument document : currentCorpus.getDocuments()) {
				Prediction multiLabelPrediction = new Prediction(model.getLabel(), document.getUsedFeatures(), document.getModelBasedClass(), document.getConfidence(), document.getDocClass());
				List<Prediction> currentPredictions = (predictions.containsKey(document.getId()))?predictions.get(document.getId()):new ArrayList<Prediction>();
				currentPredictions.add(multiLabelPrediction);
				predictions.put(document.getId(), currentPredictions);
			}
		}
		this.predictions = predictions;
	}
	/**
	 * This method merges all the predictions for all the documents
	 */
	private void mergePredictions(){
		HashMap<String, HashMap<Integer, Double>> mergedPredictions = new HashMap<>();
		for (String documentID : predictions.keySet()) {
			HashMap<Integer, Double> weightsSums = new HashMap<Integer, Double>();
			HashMap<Integer, Double> confidencesSums = new HashMap<Integer, Double>();
			for (Prediction multiLabelPrediction : predictions.get(documentID)) {
				double currentValue = (weightsSums.containsKey(multiLabelPrediction.getLabel()))?weightsSums.get(multiLabelPrediction.getLabel()):0;
				weightsSums.put(multiLabelPrediction.getLabel(), currentValue + multiLabelPrediction.getWeight());
				
				Double confidence = (multiLabelPrediction.getClassPrediction()==1)?multiLabelPrediction.getConfidence():1-multiLabelPrediction.getConfidence();
				
				currentValue = (confidencesSums.containsKey(multiLabelPrediction.getLabel()))?confidencesSums.get(multiLabelPrediction.getLabel()):0;
				confidencesSums.put(multiLabelPrediction.getLabel(), currentValue + confidence*multiLabelPrediction.getWeight());
			}
			HashMap<Integer, Double> modelBasedClases = new HashMap<Integer, Double>();
			for (Integer label : corporaStatistics.keySet()) {
				modelBasedClases.put(label, confidencesSums.get(label)/weightsSums.get(label));
				if (corporaStatistics.keySet().size()==2) {
					modelBasedClases.put((label == 1)? 0:1, 1 - modelBasedClases.get(label));
					break;
				}
			}
			mergedPredictions.put(documentID, modelBasedClases);
		}
		for (Classifiable document : originalCorpus.getDocuments()) {
			document.setModelBasedClases(mergedPredictions.get(document.getId()));
		}
	}
}
